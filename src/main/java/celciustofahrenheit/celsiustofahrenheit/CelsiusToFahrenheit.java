package celciustofahrenheit.celsiustofahrenheit;

import java.io.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;

@WebServlet(name = "CelsiusToFahrenheit", value = "/degree-calculator")
public class CelsiusToFahrenheit extends HttpServlet {
    private String message;
    private Calculator calculator;
    public void init() {
        message = "Celsius to Fahrenheit calculator";
        calculator = new Calculator();
    }

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        // Hello
        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println(" <P>Degrees Celsius: " + request.getParameter("degreesC") + "\n" + "  <P>Fahrenheit: " + calculator.celsiusToFahrenheit(request.getParameter("degreesC")) + "</BODY></HTML>");
        out.println("</body></html>");

    }

    public void destroy() {
    }
}