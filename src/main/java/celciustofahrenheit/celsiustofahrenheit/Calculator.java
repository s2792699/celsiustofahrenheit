package celciustofahrenheit.celsiustofahrenheit;

public class Calculator {
    public double celsiusToFahrenheit(String celsius) {
        if (celsius == null) {
            return 0;
        }
        else {
            double degrees = Double.parseDouble(celsius);
            double fahrenheit = degrees * (9.0/5.0) + 32.0;
            return fahrenheit;
        }
    }

}
